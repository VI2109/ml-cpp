#include "NeuralNetwork.h"

double activation_function(double x) {
    // signum
    if (x < 0) {
        return 0.0;
    } else {
        return 1.0;
    }
}

NeuralNetwork::NeuralNetwork(std::vector<int> topology, weights_type weights, std::vector<std::vector<double>> biases) {
    // initialize neurons
    for (int num_neurons_in_layer : topology) {
        _neurons.push_back({});
        for (int i = 0; i < num_neurons_in_layer; ++i) {
            _neurons.back().push_back(0.0);
        }
    }
    // copy weights and biases
    _weights = weights;
    _biases = biases;
}

std::vector<double> NeuralNetwork::get_outputs(std::vector<double> inputs) {
    auto first_layer = _neurons.begin();
    // make sure the number of inputs is correct
    if (first_layer->size() != inputs.size()) {
        return {};
    }

    *first_layer = inputs;

    // forward propagation
    for (auto layer = _neurons.begin(); layer != _neurons.end()-1; ++layer) {
        int index_of_the_layer = layer - first_layer;
        auto next_layer = layer+1;
        for (int next_layer_index = 0; next_layer_index < next_layer->size(); ++next_layer_index) {
            double neuron_sum = 0;
            for (int layer_index = 0; layer_index < layer->size(); ++layer_index) {
                neuron_sum += (*layer)[layer_index] * _weights[index_of_the_layer][next_layer_index][layer_index];
            }
            (*next_layer)[next_layer_index] = activation_function(neuron_sum + _biases[index_of_the_layer][next_layer_index]);
        }
    }

    return _neurons.back();
}