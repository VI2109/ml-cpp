#ifndef NEURAL_NETWORK
#define NEURAL_NETWORK

#include <vector>
#include <cmath>

typedef std::vector<std::vector<std::vector<double>>> weights_type;

class NeuralNetwork {
public:
    NeuralNetwork(std::vector<int> topology, weights_type weights, std::vector<std::vector<double>> biases);
    std::vector<double> get_outputs(std::vector<double> inputs);
private:
    std::vector<std::vector<double>> _neurons;
    weights_type _weights;
    std::vector<std::vector<double>> _biases;
};

#endif