#include <iostream>

#include "NeuralNetwork.h"

NeuralNetwork or_network() {
    std::vector<int> topology = {2, 1};
    
    weights_type weights;
    weights.push_back({});
    weights.back().push_back({});
    weights.back().back().push_back(1.0);
    weights.back().back().push_back(1.0);

    std::vector<std::vector<double>> biases;
    biases.push_back({});
    biases.back().push_back(-0.5);

    NeuralNetwork nn(topology, weights, biases);
    return nn;
}

NeuralNetwork xor_network() {
    std::vector<int> topology = {2, 2, 1};
    
    weights_type weights;
    weights.push_back({});
    weights.back().push_back({});
    weights.back().back().push_back(1.0);
    weights.back().back().push_back(1.0);
    weights.back().push_back({});
    weights.back().back().push_back(1.0);
    weights.back().back().push_back(1.0);
    weights.push_back({});
    weights.back().push_back({});
    weights.back().back().push_back(1.0);
    weights.back().back().push_back(-1.0);

    std::vector<std::vector<double>> biases;
    biases.push_back({});
    biases.back().push_back(-0.5);
    biases.back().push_back(-1.5);
    biases.push_back({});
    biases.back().push_back(-0.5);

    NeuralNetwork nn(topology, weights, biases);
    return nn;
}

int main() {
    NeuralNetwork or_nn = or_network();
    std::vector<std::vector<double>> or_outputs;
    or_outputs.push_back(or_nn.get_outputs({0,0}));
    or_outputs.push_back(or_nn.get_outputs({0,1}));
    or_outputs.push_back(or_nn.get_outputs({1,0}));
    or_outputs.push_back(or_nn.get_outputs({1,1}));
    for (auto outputs : or_outputs) {
        std::cout << outputs[0] << ' ';
    }
    std::cout << std::endl;

    NeuralNetwork xor_nn = xor_network();
    std::vector<std::vector<double>> xor_outputs;
    xor_outputs.push_back(xor_nn.get_outputs({0,0}));
    xor_outputs.push_back(xor_nn.get_outputs({0,1}));
    xor_outputs.push_back(xor_nn.get_outputs({1,0}));
    xor_outputs.push_back(xor_nn.get_outputs({1,1}));
    for (auto outputs : xor_outputs) {
        std::cout << outputs[0] << ' ';
    }
    std::cout << std::endl;
}